import React from "react";
import ReactDOM from "react-dom";
import "./global.css";
import App from "./app";
import * as serviceWorker from "./serviceWorker";
import WebFont from "webfontloader";
import { font } from "styles";
import "styles/fonts.css";

const { regular, medium, semibold, bold } = font.primary;

WebFont.load({
  custom: {
    families: [regular, medium, semibold, bold]
  },
  events: false,
  classes: false
});

ReactDOM.render(<App />, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
