// combines array of classNames into a string
export function cn(...args) {
  return args.filter(Boolean).join(" ");
}

export const lerp = (a, b, n) => {
  return (1 - n) * a + n * b;
};

const Helpers = {
  cn,
  lerp
};

export default Helpers;
