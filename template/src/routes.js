import React from "react";
import { Route, Switch } from "react-router-dom";

const Home = React.lazy(() => import("components/pages/home"));
const About = React.lazy(() => import("components/pages/about"));
const Work = React.lazy(() => import("components/pages/work"));
const NotFound = React.lazy(() => import("components/pages/not-found"));

const ROUTES = [
  {
    path: "/",
    key: "APP_ROOT",
    component: RenderRoutes,
    routes: [
      {
        path: "/",
        key: "APP_INDEX",
        exact: true,
        component: () => <Home />
      },
      {
        path: "/about",
        key: "ABOUT",
        exact: true,
        component: () => <About />
      },
      {
        path: "/work",
        key: "WORK_ROOT",
        component: RenderRoutes,
        routes: [
          {
            path: "/work",
            key: "WORK_INDEX",
            exact: true,
            component: () => <Work />
          }
        ]
      }
    ]
  }
];

export default ROUTES;

/**
 * Render a route with potential sub routes
 * https://reacttraining.com/react-router/web/example/route-config
 */
function RouteWithSubRoutes(route) {
  return (
    <Route path={route.path} exact={route.exact}>
      <route.component routes={route.routes} />
    </Route>
  );
}

/**
 * Use this component for any new section of routes (any config object that has a "routes" property
 */
export function RenderRoutes({ routes }) {
  return (
    <Switch>
      {routes.map((route, i) => {
        return <RouteWithSubRoutes key={route.key} {...route} />;
      })}
      <Route component={() => <NotFound />} />
    </Switch>
  );
}
