import textStyles from "components/typography/typography.module.scss";
import breakpoints from "styles/responsive.scss";
import projectColors from "styles/vars.scss";

// Size Unit
export const unit = "rem";

// Colors
export const white = projectColors.white;
export const black = projectColors.black;
export const brandYellow = projectColors.brandYellow;
export const brandPink = projectColors.brandPink;
export const brandRed = projectColors.brandRed;
export const brandTeal = projectColors.brandTeal;
export const brandPurple = projectColors.brandPurple;
export const brandGreen = projectColors.brandGreen;

export const color = {
  white: white,
  black: black,
  brandYellow: brandYellow,
  brandPink: brandPink,
  brandRed: brandRed,
  brandTeal: brandTeal,
  brandPurple: brandPurple,
  brandGreen: brandGreen
};

// Breakpoints
export const breakPoints = {
  mobile: breakpoints.mobile,
  tablet: breakpoints.tablet,
  desktop: breakpoints.desktop,
  widescreen: breakpoints.widescreen
};

// Fonts
export const font = {
  primary: {
    regular: "Faktum-Regular",
    medium: "Faktum-Medium",
    semibold: "Faktum-SemiBold",
    bold: "Faktum-Bold"
  },
  mono: "monospace"
};

// Font Sizes
export const fontSizes = {
  body: {
    default: textStyles.body_default,
    big: textStyles.body_big
  },
  headline: {
    default: textStyles.headline_default,
    big: textStyles.headline_big,
    display: textStyles.headline_display
  }
};

// Spacing (Paddings/Margins)
export const space = [0, 1, 3, 10];

const styles = {
  bold: 600,
  space,
  color,
  font,
  unit,
  fontSizes,
  breakPoints
};

export default styles;
