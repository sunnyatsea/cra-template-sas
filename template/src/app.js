import React, { Suspense } from "react";
import { Router } from "react-router-dom";
import { createBrowserHistory } from "history";
import LoadingSpinner from "components/ui-components/loading-spinner";
import ErrorBoundary from "components/pages/error-boundary";
import ROUTES, { RenderRoutes } from "./routes";
import styles from "styles";

const Header = React.lazy(() => import("components/ui-components/header"));
const SEO = React.lazy(() => import("components/utility-wrappers/seo"));
const history = createBrowserHistory();

function App() {
  return (
    <Suspense fallback={<LoadingSpinner />}>
      <SEO
        bodyAttrs={{ style: `background: ${styles.color.black}` }}
        title="Home"
      />
      <Router history={history}>
        <Header />
        <Suspense fallback={<LoadingSpinner />}>
          <ErrorBoundary>
            <RenderRoutes routes={ROUTES} />
          </ErrorBoundary>
        </Suspense>
      </Router>
    </Suspense>
  );
}

export default App;
