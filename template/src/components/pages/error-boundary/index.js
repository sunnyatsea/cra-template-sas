import React from "react";
import Box from "components/ui-components/box";
import { CenterXY } from "components/utility-wrappers/center";
import { HeadlineBig } from "components/typography";
import { white, brandRed } from "styles";

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    console.log(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <Box
          style={{ height: "100%", width: "100%", backgroundColor: brandRed }}
        >
          <CenterXY>
            <HeadlineBig tag="h5" textColor={white}>
              Oops, a part of the application crashed.
            </HeadlineBig>
          </CenterXY>
        </Box>
      );
    }

    return this.props.children;
  }
}
