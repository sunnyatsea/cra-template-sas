import React from "react";
import styles from "./home.module.scss";
import { HeadlineBig } from "components/typography";
import Box from "components/ui-components/box";
import Center from "components/utility-wrappers/center";
import SEO from "components/utility-wrappers/seo";

export default function Home() {
  return (
    <>
      <SEO title="Home" />
      <Box className={styles.root}>
        <Center xy>
          <HeadlineBig center>Home.</HeadlineBig>
        </Center>
      </Box>
    </>
  );
}
