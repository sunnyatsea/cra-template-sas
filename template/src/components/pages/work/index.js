import React from "react";
import styles from "./work.module.scss";
import { HeadlineBig } from "components/typography";
import Box from "components/ui-components/box";
import Center from "components/utility-wrappers/center";
import SEO from "components/utility-wrappers/seo";

export default function Work() {
  return (
    <>
      <SEO title="Work" />
      <Box className={styles.root}>
        <Center xy>
          <HeadlineBig center>Work.</HeadlineBig>
        </Center>
      </Box>
    </>
  );
}
