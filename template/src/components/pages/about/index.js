import React from "react";
import styles from "./about.module.scss";
import { HeadlineBig } from "components/typography";
import Box from "components/ui-components/box";
import Center from "components/utility-wrappers/center";
import SEO from "components/utility-wrappers/seo";

export default function About() {
  return (
    <>
      <SEO title="About" />
      <Box className={styles.root}>
        <Center xy>
          <HeadlineBig center>About.</HeadlineBig>
        </Center>
      </Box>
    </>
  );
}
