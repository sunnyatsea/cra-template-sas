import React from "react";
import Box from "components/ui-components/box";
import { CenterXY } from "components/utility-wrappers/center";
import SEO from "components/utility-wrappers/seo";
import { HeadlineBig, ParagraphBig } from "components/typography";
import { white, brandRed } from "styles";

export default function NotFound() {
  return (
    <>
      <SEO title="Work" />
      <Box style={{ height: "100%", width: "100%", backgroundColor: brandRed }}>
        <CenterXY>
          <Box p={2} style={{ width: "100%", maxWidth: "100rem" }}>
            <HeadlineBig
              tag="h1"
              textColor={white}
              style={{ marginBottom: "3rem" }}
            >
              The contents of this page seems to have wandered off.
            </HeadlineBig>
            <ParagraphBig textColor={white}>
              You know, like the paintings in Harry Potter.
            </ParagraphBig>
          </Box>
        </CenterXY>
      </Box>
    </>
  );
}
