import React from "react";
import PropTypes from "prop-types";
import Helmet from "react-helmet";

const SEO = ({ description, meta, keywords = [], title, lang, bodyAttrs }) => {
  const metaDescription = description || "";
  return (
    <Helmet
      htmlAttributes={{
        lang
      }}
      bodyAttributes={bodyAttrs}
      title={title ? title : "Sunny at Sea"}
      titleTemplate={title ? "%s | Sunny at Sea" : "Sunny at Sea"}
      meta={[
        {
          charSet: "utf-8"
        },
        {
          name: "description",
          content: metaDescription
        },
        {
          property: "og:title",
          content: title
        },
        {
          property: "og:description",
          content: metaDescription
        },
        {
          property: "og:type",
          content: "website"
        },
        {
          name: "twitter:card",
          content: "summary"
        },
        {
          name: "twitter:creator",
          content: "Sunny at Sea"
        },
        {
          name: "twitter:title",
          content: title
        },
        {
          name: "twitter:description",
          content: metaDescription
        }
      ]
        .concat(
          keywords && keywords.length > 0
            ? {
                name: "keywords",
                content: keywords.join(", ")
              }
            : []
        )
        .concat(meta)}
    />
  );
};

export default SEO;

SEO.defaultProps = {
  lang: "en",
  meta: [],
  keywords: []
};

SEO.propTypes = {
  description: PropTypes.string,
  lang: PropTypes.string,
  meta: PropTypes.array,
  keywords: PropTypes.arrayOf(PropTypes.string),
  title: PropTypes.string.isRequired
};
