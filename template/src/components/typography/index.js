import React from "react";
import { font, fontSizes } from "styles";

export const Text = ({
  textColor,
  tag = "span",
  fontFamily,
  center,
  caps,
  capitalize,
  ...props
}) => {
  const Tag = tag;
  const textStyle = {
    color: textColor ? textColor : null,
    fontFamily: fontFamily ? fontFamily : null,
    textAlign: center ? "center" : null,
    textTransform: caps ? "uppercase" : capitalize ? "capitalize" : null
  };
  const mergedStyle = Object.assign(textStyle, props.style);

  return <Tag {...props} style={mergedStyle} />;
};

export const Headline = props => {
  const { className, ...rest } = props || {};
  return (
    <Text
      {...rest}
      fontFamily={font.primary.semibold}
      className={`${fontSizes.headline.default} ${className ? className : ""}`}
      tag="h1"
    />
  );
};

export const HeadlineBig = props => {
  const { className, ...rest } = props || {};
  return (
    <Text
      {...rest}
      fontFamily={font.primary.semibold}
      className={`${fontSizes.headline.big} ${className ? className : ""}`}
      tag="h2"
    />
  );
};

export const HeadlineDisplay = props => {
  const { className, ...rest } = props || {};
  return (
    <Text
      {...rest}
      fontFamily={font.primary.semibold}
      className={`${fontSizes.headline.display} ${className ? className : ""}`}
      tag="h1"
    />
  );
};

export const Paragraph = props => {
  const { className, ...rest } = props || {};
  return (
    <Text
      {...rest}
      className={`${fontSizes.body.default} ${className ? className : ""}`}
      tag="p"
    />
  );
};

export const ParagraphBig = props => {
  const { className, ...rest } = props || {};
  return (
    <Text
      {...rest}
      className={`${fontSizes.body.big} ${className ? className : ""}`}
      tag="p"
    />
  );
};

export const Code = props => {
  const { className, ...rest } = props || {};
  return (
    <Text
      {...rest}
      className={`${fontSizes.body.default} ${className ? className : ""}`}
      tag="code"
      fontFamily={font.mono}
    />
  );
};
