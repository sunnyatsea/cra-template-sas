import React from "react";
import { space, unit } from "styles";

export default function Box({ tag = "div", m, p, style, ...props }) {
  const Tag = tag;
  const boxStyle = {
    margin: m ? `${space[m]}${unit}` : null,
    padding: p ? `${space[p]}${unit}` : null
  };
  const mergedStyle = Object.assign(boxStyle, style);

  return <Tag {...props} style={mergedStyle} />;
}
