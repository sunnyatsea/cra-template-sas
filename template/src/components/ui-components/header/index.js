import React from "react";
import { Link } from "react-router-dom";
import { ParagraphBig } from "components/typography";
import Icon from "components/ui-components/icon";
import { font, black } from "styles";
import styles from "./header.module.scss";

const AnimatedLink = React.lazy(() =>
  import("components/ui-components/animated-link")
);

const linkStyle = { marginLeft: "5rem" };

const Header = props => {
  return (
    <>
      <Link to="/">
        <Icon symbol="logo" className={styles.logoContainer} />
      </Link>
      <div className={styles.header}>
        <AnimatedLink data-sticky-cursor style={linkStyle} to="/about">
          <ParagraphBig fontFamily={font.primary.semibold} textColor={black}>
            About.
          </ParagraphBig>
        </AnimatedLink>
        <AnimatedLink data-sticky-cursor style={linkStyle} to="/work">
          <ParagraphBig fontFamily={font.primary.semibold} textColor={black}>
            Work.
          </ParagraphBig>
        </AnimatedLink>
      </div>
    </>
  );
};

export default Header;
