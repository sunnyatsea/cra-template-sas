import React, { useEffect, useState } from "react";
import styles from "./loading-spinner.module.scss";
import Icon from "components/ui-components/icon";
import { CenterXY } from "components/utility-wrappers/center";

function LoadingSpinner() {
  const [isLoaded, setIsLoaded] = useState(false);

  useEffect(() => {
    let loadedTimer = setTimeout(() => setIsLoaded(true), 1000);

    return () => {
      clearTimeout(loadedTimer);
    };
  }, []);

  return (
    <CenterXY>
      <Icon
        style={isLoaded ? { opacity: 1.0 } : { opacity: 0 }}
        symbol="spinner"
        className={styles.spinner}
      />
    </CenterXY>
  );
}

export default LoadingSpinner;
