# Sunny at Sea React Starter Template

An opinionated CRA starter template that has been setup with:

- router configs
- errorboundary
- react-helmet
- webfontloader
- starter components
- styles
- custom hooks

## Install

`npx create-react-app my-app --template sas`

## Development Workflow

1. Update fonts in `/styles/fonts.css` and `/styles/fonts`.
2. Start by changing all the variables in `/styles/vars.scss`, `/styles/index.js` and `/styles/responsive.scss` to fit your project.
3. Take a look at the utility wrappers and custom hooks that are available.
4. Change or set up new routes (pages) in `/routes.js` and `components/pages`.
5. Update the start components in `/components/ui-components`.
6. Don't forget to customize the error-boundary, `package.json`, README.md, `/public/manifest.json` and other files in `/public`.
